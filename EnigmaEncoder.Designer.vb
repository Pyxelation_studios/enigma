﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class enigmaEncoder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tbInput = New System.Windows.Forms.TextBox()
        Me.tbOutput = New System.Windows.Forms.TextBox()
        Me.nudFirstRotor = New System.Windows.Forms.NumericUpDown()
        Me.gbSettings = New System.Windows.Forms.GroupBox()
        Me.gbRingSettings = New System.Windows.Forms.GroupBox()
        Me.lbRingSetting1 = New System.Windows.Forms.Label()
        Me.lbRingSetting3 = New System.Windows.Forms.Label()
        Me.nudRingSetting1 = New System.Windows.Forms.NumericUpDown()
        Me.lbRingSetting2 = New System.Windows.Forms.Label()
        Me.nudRingSetting2 = New System.Windows.Forms.NumericUpDown()
        Me.nudRingSetting3 = New System.Windows.Forms.NumericUpDown()
        Me.cbActive = New System.Windows.Forms.CheckBox()
        Me.btnPlugboardChange = New System.Windows.Forms.Button()
        Me.gbRotors = New System.Windows.Forms.GroupBox()
        Me.btnRotorsChange = New System.Windows.Forms.Button()
        Me.lbRotor1 = New System.Windows.Forms.Label()
        Me.lbRotor3 = New System.Windows.Forms.Label()
        Me.lbRotor2 = New System.Windows.Forms.Label()
        Me.nudSecondRotor = New System.Windows.Forms.NumericUpDown()
        Me.nudThirdRotor = New System.Windows.Forms.NumericUpDown()
        Me.DeltaTime = New System.Windows.Forms.Timer(Me.components)
        Me.lbInput = New System.Windows.Forms.Label()
        CType(Me.nudFirstRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSettings.SuspendLayout()
        Me.gbRingSettings.SuspendLayout()
        CType(Me.nudRingSetting1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRingSetting2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRingSetting3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRotors.SuspendLayout()
        CType(Me.nudSecondRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudThirdRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbInput
        '
        Me.tbInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.tbInput.Location = New System.Drawing.Point(12, 15)
        Me.tbInput.Multiline = True
        Me.tbInput.Name = "tbInput"
        Me.tbInput.ReadOnly = True
        Me.tbInput.ShortcutsEnabled = False
        Me.tbInput.Size = New System.Drawing.Size(920, 180)
        Me.tbInput.TabIndex = 0
        '
        'tbOutput
        '
        Me.tbOutput.Location = New System.Drawing.Point(12, 309)
        Me.tbOutput.Multiline = True
        Me.tbOutput.Name = "tbOutput"
        Me.tbOutput.ReadOnly = True
        Me.tbOutput.Size = New System.Drawing.Size(920, 180)
        Me.tbOutput.TabIndex = 1
        '
        'nudFirstRotor
        '
        Me.nudFirstRotor.Location = New System.Drawing.Point(8, 42)
        Me.nudFirstRotor.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudFirstRotor.Name = "nudFirstRotor"
        Me.nudFirstRotor.Size = New System.Drawing.Size(82, 20)
        Me.nudFirstRotor.TabIndex = 3
        Me.nudFirstRotor.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'gbSettings
        '
        Me.gbSettings.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.gbSettings.Controls.Add(Me.gbRingSettings)
        Me.gbSettings.Controls.Add(Me.cbActive)
        Me.gbSettings.Controls.Add(Me.btnPlugboardChange)
        Me.gbSettings.Controls.Add(Me.gbRotors)
        Me.gbSettings.Location = New System.Drawing.Point(12, 201)
        Me.gbSettings.Name = "gbSettings"
        Me.gbSettings.Size = New System.Drawing.Size(920, 100)
        Me.gbSettings.TabIndex = 5
        Me.gbSettings.TabStop = False
        Me.gbSettings.Text = "Settings"
        '
        'gbRingSettings
        '
        Me.gbRingSettings.Controls.Add(Me.lbRingSetting1)
        Me.gbRingSettings.Controls.Add(Me.lbRingSetting3)
        Me.gbRingSettings.Controls.Add(Me.nudRingSetting1)
        Me.gbRingSettings.Controls.Add(Me.lbRingSetting2)
        Me.gbRingSettings.Controls.Add(Me.nudRingSetting2)
        Me.gbRingSettings.Controls.Add(Me.nudRingSetting3)
        Me.gbRingSettings.Location = New System.Drawing.Point(637, 20)
        Me.gbRingSettings.Name = "gbRingSettings"
        Me.gbRingSettings.Size = New System.Drawing.Size(277, 74)
        Me.gbRingSettings.TabIndex = 13
        Me.gbRingSettings.TabStop = False
        Me.gbRingSettings.Text = "Rings:"
        '
        'lbRingSetting1
        '
        Me.lbRingSetting1.AutoSize = True
        Me.lbRingSetting1.Location = New System.Drawing.Point(6, 26)
        Me.lbRingSetting1.Name = "lbRingSetting1"
        Me.lbRingSetting1.Size = New System.Drawing.Size(49, 13)
        Me.lbRingSetting1.TabIndex = 7
        Me.lbRingSetting1.Text = "First ring:"
        '
        'lbRingSetting3
        '
        Me.lbRingSetting3.AutoSize = True
        Me.lbRingSetting3.Location = New System.Drawing.Point(183, 26)
        Me.lbRingSetting3.Name = "lbRingSetting3"
        Me.lbRingSetting3.Size = New System.Drawing.Size(54, 13)
        Me.lbRingSetting3.TabIndex = 9
        Me.lbRingSetting3.Text = "Third ring:"
        '
        'nudRingSetting1
        '
        Me.nudRingSetting1.Location = New System.Drawing.Point(8, 42)
        Me.nudRingSetting1.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudRingSetting1.Name = "nudRingSetting1"
        Me.nudRingSetting1.Size = New System.Drawing.Size(82, 20)
        Me.nudRingSetting1.TabIndex = 3
        Me.nudRingSetting1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lbRingSetting2
        '
        Me.lbRingSetting2.AutoSize = True
        Me.lbRingSetting2.Location = New System.Drawing.Point(93, 26)
        Me.lbRingSetting2.Name = "lbRingSetting2"
        Me.lbRingSetting2.Size = New System.Drawing.Size(67, 13)
        Me.lbRingSetting2.TabIndex = 8
        Me.lbRingSetting2.Text = "Second ring:"
        '
        'nudRingSetting2
        '
        Me.nudRingSetting2.Location = New System.Drawing.Point(96, 42)
        Me.nudRingSetting2.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudRingSetting2.Name = "nudRingSetting2"
        Me.nudRingSetting2.Size = New System.Drawing.Size(82, 20)
        Me.nudRingSetting2.TabIndex = 4
        Me.nudRingSetting2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'nudRingSetting3
        '
        Me.nudRingSetting3.Location = New System.Drawing.Point(184, 42)
        Me.nudRingSetting3.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudRingSetting3.Name = "nudRingSetting3"
        Me.nudRingSetting3.Size = New System.Drawing.Size(82, 20)
        Me.nudRingSetting3.TabIndex = 5
        Me.nudRingSetting3.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cbActive
        '
        Me.cbActive.AutoSize = True
        Me.cbActive.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.cbActive.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.cbActive.FlatAppearance.BorderSize = 2
        Me.cbActive.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.cbActive.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.cbActive.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbActive.Location = New System.Drawing.Point(536, 46)
        Me.cbActive.Name = "cbActive"
        Me.cbActive.Size = New System.Drawing.Size(62, 18)
        Me.cbActive.TabIndex = 12
        Me.cbActive.Text = "Active"
        Me.cbActive.UseVisualStyleBackColor = False
        '
        'btnPlugboardChange
        '
        Me.btnPlugboardChange.Location = New System.Drawing.Point(404, 24)
        Me.btnPlugboardChange.Name = "btnPlugboardChange"
        Me.btnPlugboardChange.Size = New System.Drawing.Size(116, 70)
        Me.btnPlugboardChange.TabIndex = 11
        Me.btnPlugboardChange.Text = "Change plugboard settings"
        Me.btnPlugboardChange.UseVisualStyleBackColor = True
        '
        'gbRotors
        '
        Me.gbRotors.Controls.Add(Me.btnRotorsChange)
        Me.gbRotors.Controls.Add(Me.lbRotor1)
        Me.gbRotors.Controls.Add(Me.lbRotor3)
        Me.gbRotors.Controls.Add(Me.nudFirstRotor)
        Me.gbRotors.Controls.Add(Me.lbRotor2)
        Me.gbRotors.Controls.Add(Me.nudSecondRotor)
        Me.gbRotors.Controls.Add(Me.nudThirdRotor)
        Me.gbRotors.Location = New System.Drawing.Point(6, 20)
        Me.gbRotors.Name = "gbRotors"
        Me.gbRotors.Size = New System.Drawing.Size(364, 74)
        Me.gbRotors.TabIndex = 10
        Me.gbRotors.TabStop = False
        Me.gbRotors.Text = "Rotors:"
        '
        'btnRotorsChange
        '
        Me.btnRotorsChange.Location = New System.Drawing.Point(272, 39)
        Me.btnRotorsChange.Name = "btnRotorsChange"
        Me.btnRotorsChange.Size = New System.Drawing.Size(82, 23)
        Me.btnRotorsChange.TabIndex = 10
        Me.btnRotorsChange.Text = "Change rotors"
        Me.btnRotorsChange.UseVisualStyleBackColor = True
        '
        'lbRotor1
        '
        Me.lbRotor1.AutoSize = True
        Me.lbRotor1.Location = New System.Drawing.Point(6, 26)
        Me.lbRotor1.Name = "lbRotor1"
        Me.lbRotor1.Size = New System.Drawing.Size(53, 13)
        Me.lbRotor1.TabIndex = 7
        Me.lbRotor1.Text = "First rotor:"
        '
        'lbRotor3
        '
        Me.lbRotor3.AutoSize = True
        Me.lbRotor3.Location = New System.Drawing.Point(183, 26)
        Me.lbRotor3.Name = "lbRotor3"
        Me.lbRotor3.Size = New System.Drawing.Size(58, 13)
        Me.lbRotor3.TabIndex = 9
        Me.lbRotor3.Text = "Third rotor:"
        '
        'lbRotor2
        '
        Me.lbRotor2.AutoSize = True
        Me.lbRotor2.Location = New System.Drawing.Point(93, 26)
        Me.lbRotor2.Name = "lbRotor2"
        Me.lbRotor2.Size = New System.Drawing.Size(71, 13)
        Me.lbRotor2.TabIndex = 8
        Me.lbRotor2.Text = "Second rotor:"
        '
        'nudSecondRotor
        '
        Me.nudSecondRotor.Location = New System.Drawing.Point(96, 42)
        Me.nudSecondRotor.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudSecondRotor.Name = "nudSecondRotor"
        Me.nudSecondRotor.Size = New System.Drawing.Size(82, 20)
        Me.nudSecondRotor.TabIndex = 4
        Me.nudSecondRotor.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'nudThirdRotor
        '
        Me.nudThirdRotor.Location = New System.Drawing.Point(184, 42)
        Me.nudThirdRotor.Maximum = New Decimal(New Integer() {27, 0, 0, 0})
        Me.nudThirdRotor.Name = "nudThirdRotor"
        Me.nudThirdRotor.Size = New System.Drawing.Size(82, 20)
        Me.nudThirdRotor.TabIndex = 5
        Me.nudThirdRotor.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'DeltaTime
        '
        Me.DeltaTime.Enabled = True
        Me.DeltaTime.Interval = 1
        '
        'lbInput
        '
        Me.lbInput.AutoSize = True
        Me.lbInput.Location = New System.Drawing.Point(13, 1)
        Me.lbInput.Name = "lbInput"
        Me.lbInput.Size = New System.Drawing.Size(34, 13)
        Me.lbInput.TabIndex = 6
        Me.lbInput.Text = "Input:"
        '
        'enigmaEncoder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 501)
        Me.Controls.Add(Me.lbInput)
        Me.Controls.Add(Me.gbSettings)
        Me.Controls.Add(Me.tbOutput)
        Me.Controls.Add(Me.tbInput)
        Me.Name = "enigmaEncoder"
        Me.ShowIcon = False
        Me.Text = "Enigma machine"
        CType(Me.nudFirstRotor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSettings.ResumeLayout(False)
        Me.gbSettings.PerformLayout()
        Me.gbRingSettings.ResumeLayout(False)
        Me.gbRingSettings.PerformLayout()
        CType(Me.nudRingSetting1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRingSetting2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRingSetting3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRotors.ResumeLayout(False)
        Me.gbRotors.PerformLayout()
        CType(Me.nudSecondRotor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudThirdRotor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbInput As TextBox
    Friend WithEvents tbOutput As TextBox
    Friend WithEvents nudFirstRotor As NumericUpDown
    Friend WithEvents gbSettings As GroupBox
    Friend WithEvents nudThirdRotor As NumericUpDown
    Friend WithEvents nudSecondRotor As NumericUpDown
    Friend WithEvents lbRotor3 As Label
    Friend WithEvents lbRotor2 As Label
    Friend WithEvents lbRotor1 As Label
    Friend WithEvents gbRotors As GroupBox
    Friend WithEvents btnRotorsChange As Button
    Friend WithEvents DeltaTime As Timer
    Friend WithEvents btnPlugboardChange As Button
    Friend WithEvents lbInput As Label
    Friend WithEvents cbActive As CheckBox
    Friend WithEvents gbRingSettings As GroupBox
    Friend WithEvents lbRingSetting1 As Label
    Friend WithEvents lbRingSetting3 As Label
    Friend WithEvents nudRingSetting1 As NumericUpDown
    Friend WithEvents lbRingSetting2 As Label
    Friend WithEvents nudRingSetting2 As NumericUpDown
    Friend WithEvents nudRingSetting3 As NumericUpDown
End Class
