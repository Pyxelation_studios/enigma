﻿Public Class enigmaEncoder
    'Algemene variabelen
    Private onlyOne As Boolean = False
    Public lastAdded As Integer
    Public lValue1 As Integer = 1
    Public lValue2 As Integer = 1

    'Rotor settings
    Public firstRotor As Integer = 1
    Public secondRotor As Integer = 2
    Public thirdRotor As Integer = 3

    'Plugboard settings
    Public plugboardString As String = ""

    'The rotors
    ''Arrays zijn offsets van letters binnen een rotor, bijv. a -> g = 6
    'rotor1
    Public Rotor1_1 = New Integer() {4, 9, 10, 2, 7, 1, 23, 9, 13, 16, 3, 8, 2, 9, 10, 18, 7, 3, 0, 22, 6, 13, 5, 20, 4, 10}
    Public Rotor1_2 = New Integer() {6, 5, 4, 23, 4, 2, 1, 18, 13, 10, 9, 7, 10, 3, 2, 22, 9, 20, 0, 8, 3, 13, 9, 7, 10, 16}
    'Object array rotor 1 [heenweg, terugweg, knockover]
    Public Rotor1 = New Object() {Rotor1_1, Rotor1_2, 18}
    ' rotor2
    Public Rotor2_1 = New Integer() {0, 8, 1, 7, 14, 3, 11, 13, 15, 18, 1, 22, 10, 6, 24, 13, 0, 15, 7, 20, 21, 3, 9, 24, 16, 5}
    Public Rotor2_2 = New Integer() {0, 18, 13, 1, 5, 9, 15, 22, 3, 8, 7, 1, 24, 20, 16, 21, 0, 11, 14, 6, 13, 24, 10, 15, 3, 7}
    'Object array rotor 1 [heenweg, terugweg, knockover]
    Public Rotor2 = New Object() {Rotor2_1, Rotor2_2, 6}
    'rotor3
    Public Rotor3_1 = New Integer() {1, 2, 3, 4, 5, 6, 22, 8, 9, 10, 13, 10, 13, 0, 10, 15, 18, 5, 14, 7, 16, 17, 24, 21, 18, 15}
    Public Rotor3_2 = New Integer() {7, 1, 22, 2, 15, 3, 14, 4, 18, 5, 16, 6, 17, 0, 15, 8, 18, 9, 21, 10, 24, 10, 5, 13, 10, 13}
    'Object array rotor 1 [heenweg, terugweg, knockover]
    Public Rotor3 = New Object() {Rotor3_1, Rotor3_2, 23}
    'rotor4
    Public Rotor4_1 = New Integer() {4, 17, 12, 18, 11, 20, 3, 19, 14, 7, 10, 23, 5, 20, 9, 22, 23, 14, 1, 13, 16, 8, 6, 15, 24, 2}
    Public Rotor4_2 = New Integer() {19, 2, 6, 8, 4, 14, 13, 20, 23, 3, 16, 22, 15, 23, 12, 11, 7, 5, 17, 1, 10, 18, 24, 9, 16, 20}
    'Object array rotor 1 [heenweg, terugweg, knockover]
    Public Rotor4 = New Object() {Rotor4_1, Rotor4_2, 11}
    'rotor5
    Public Rotor5_1 = New Integer() {21, 24, 25, 14, 2, 3, 13, 17, 12, 6, 8, 18, 1, 20, 23, 8, 10, 5, 20, 16, 22, 19, 9, 7, 4, 11}
    Public Rotor5_2 = New Integer() {10, 25, 4, 18, 7, 9, 2, 20, 3, 16, 11, 23, 20, 1, 19, 6, 22, 14, 8, 13, 12, 21, 5, 8, 17, 24}
    'Object array rotor 1 [heenweg, terugweg, knockover]
    Public Rotor5 = New Object() {Rotor5_1, Rotor5_2, 1}
    'rotors array including all rotors
    Public Rotors = New Object() {Rotor1, Rotor2, Rotor3, Rotor4, Rotor5}

    Private Sub nudRotor_ValueChanged(sender As Object, e As EventArgs) Handles nudFirstRotor.ValueChanged, nudSecondRotor.ValueChanged, nudThirdRotor.ValueChanged
        If cbActive.Checked Then
            If sender.Equals(nudFirstRotor) Then
                Dim rotor = Rotors(firstRotor - 1)
                Dim knockover As Integer = rotor(2) + (nudRingSetting1.Value - 1)
                knockover = cirkelLoop(knockover, 1, 26)

                If nudFirstRotor.Value > lValue1 Then
                    If nudFirstRotor.Value = knockover Then
                        nudSecondRotor.Value += 1
                    End If
                Else
                    If nudFirstRotor.Value = knockover - 1 Then
                        nudSecondRotor.Value -= 1
                    End If
                End If
            End If
            If sender.Equals(nudSecondRotor) Then
                Dim rotor = Rotors(secondRotor - 1)
                Dim knockover As Integer = rotor(2) + (nudRingSetting2.Value - 1)
                knockover = cirkelLoop(knockover, 1, 26)

                If nudSecondRotor.Value > lValue2 Then
                    If nudSecondRotor.Value = knockover Then
                        nudThirdRotor.Value += 1
                    End If
                Else
                    If nudSecondRotor.Value = knockover - 1 Then
                        nudThirdRotor.Value -= 1
                    End If
                End If
            End If
        End If
        lValue1 = nudFirstRotor.Value
        nudFirstRotor.Value = cirkelLoop(nudFirstRotor.Value, 1, 26)
        lValue2 = nudSecondRotor.Value
        nudSecondRotor.Value = cirkelLoop(nudSecondRotor.Value, 1, 26)
        nudThirdRotor.Value = cirkelLoop(nudThirdRotor.Value, 1, 26)
    End Sub

    Private Sub nudRingSetting_ValueChanged(sender As Object, e As EventArgs) Handles nudRingSetting1.ValueChanged, nudRingSetting2.ValueChanged, nudRingSetting3.ValueChanged
        sender.value = cirkelLoop(sender.value, 1, 26)
    End Sub

    Private Sub tbInput_characterCheck(sender As Object, e As KeyPressEventArgs) Handles tbInput.KeyPress
        If cbActive.Checked Then
            If Not (Asc(e.KeyChar) = 8) Then 'not backspace
                Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
                If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                    e.KeyChar = ChrW(0)
                    e.Handled = True
                Else
                    nudFirstRotor.Value += 1
                    lastAdded = Asc(e.KeyChar)
                End If
            Else
                If tbInput.Text.Length > 0 Then
                    nudFirstRotor.Value -= 1
                    Dim str As String = ""
                    For i As Integer = 0 To tbOutput.Text.Length - 2
                        str += tbOutput.Text(i)
                    Next
                    lastAdded = Asc(e.KeyChar)
                    tbOutput.Text = str
                End If
            End If
        End If
    End Sub

    Private Sub cbActive_CheckedChanged(sender As Object, e As EventArgs) Handles cbActive.CheckedChanged
        tbInput.ReadOnly = Not cbActive.Checked
        gbRotors.Enabled = Not cbActive.Checked
        gbRingSettings.Enabled = Not cbActive.Checked
        btnPlugboardChange.Enabled = Not cbActive.Checked
        If cbActive.Checked = False Then
            tbInput.Text = ""
            tbOutput.Text = ""
        Else
            Plugboard.Close()
            ChangeRotors.Close()
        End If
    End Sub

    Private Sub ProgramLoop() Handles tbInput.TextChanged
        If cbActive.Checked And Not lastAdded = 8 Then 'not backspace
            Dim str As String = tbInput.Text
            Console.WriteLine("Input: " + str + ", Length: " + str.Length.ToString())
            Dim newChar As Integer = Asc(str(str.Length - 1))
            Console.WriteLine("Char: " + Chr(newChar).ToString())

            ''Plugboard
            If plugboardString.Contains(Chr(newChar)) Then
                Console.WriteLine("Character config in plugboard!")
                Dim plugboardArray As String() = Split(plugboardString, "/")
                Dim link As String = ""
                For i As Integer = 0 To plugboardArray.Length - 1
                    If plugboardArray(i).Contains(Chr(newChar)) Then
                        link = plugboardArray(i)
                    End If
                Next
                If link.IndexOf(Chr(newChar)) = 0 Then
                    newChar = Asc(link(2))
                ElseIf link.IndexOf(Chr(newChar)) = 2 Then
                    newChar = Asc(link(0))
                End If
                Console.WriteLine("New char: " + Chr(newChar).ToString())
            End If

            ''1st time going through the rotors
            '1st rotor
            Dim contact = (newChar - 96) + (nudFirstRotor.Value - 1) + (nudRingSetting1.Value - 1)
            contact = cirkelLoop(contact, 1, 26)
            Console.WriteLine("Contact: " + contact.ToString())
            newChar += Rotors(firstRotor - 1)(0)(contact - 1)
            newChar = cirkelLoop(newChar, 97, 122)
            Console.WriteLine("Offset: " + Rotors(firstRotor - 1)(0)(contact - 1).ToString())
            Console.WriteLine("New char: " + Chr(newChar).ToString())

            If Not onlyOne Then
                '2nd rotor
                contact = (newChar - 96) + (nudSecondRotor.Value - 1) + (nudRingSetting2.Value - 1)
                contact = cirkelLoop(contact, 1, 26)
                Console.WriteLine("Contact: " + contact.ToString())
                newChar += Rotors(secondRotor - 1)(0)(contact - 1)
                newChar = cirkelLoop(newChar, 97, 122)
                Console.WriteLine("Offset: " + Rotors(secondRotor - 1)(0)(contact - 1).ToString())
                Console.WriteLine("New char: " + Chr(newChar).ToString())

                '3rd rotor
                contact = (newChar - 96) + (nudThirdRotor.Value - 1) + (nudRingSetting3.Value - 1)
                contact = cirkelLoop(contact, 1, 26)
                Console.WriteLine("Contact: " + contact.ToString())
                newChar += Rotors(thirdRotor - 1)(0)(contact - 1)
                newChar = cirkelLoop(newChar, 97, 122)
                Console.WriteLine("Offset: " + Rotors(thirdRotor - 1)(0)(contact - 1).ToString())
                Console.WriteLine("New char: " + Chr(newChar).ToString())
            End If

            ''Reflectors
            If True Then
                '(AY)
                If newChar = 97 Then
                    newChar = 121
                ElseIf newChar = 121 Then
                    newChar = 97
                End If

                '(BR)
                If newChar = 98 Then
                    newChar = 114
                ElseIf newChar = 114 Then
                    newChar = 98
                End If

                '(CU)
                If newChar = 99 Then
                    newChar = 117
                ElseIf newChar = 117 Then
                    newChar = 99
                End If

                '(DH)
                If newChar = 100 Then
                    newChar = 104
                ElseIf newChar = 104 Then
                    newChar = 100
                End If

                '(EQ)
                If newChar = 101 Then
                    newChar = 113
                ElseIf newChar = 113 Then
                    newChar = 101
                End If

                '(FS)
                If newChar = 102 Then
                    newChar = 115
                ElseIf newChar = 115 Then
                    newChar = 102
                End If

                '(GL)
                If newChar = 103 Then
                    newChar = 108
                ElseIf newChar = 108 Then
                    newChar = 103
                End If

                '(IP)
                If newChar = 105 Then
                    newChar = 112
                ElseIf newChar = 112 Then
                    newChar = 105
                End If

                '(JX)
                If newChar = 106 Then
                    newChar = 120
                ElseIf newChar = 120 Then
                    newChar = 106
                End If

                '(KN)
                If newChar = 107 Then
                    newChar = 110
                ElseIf newChar = 110 Then
                    newChar = 107
                End If

                '(MO)
                If newChar = 109 Then
                    newChar = 111
                ElseIf newChar = 111 Then
                    newChar = 109
                End If

                '(TZ)
                If newChar = 116 Then
                    newChar = 122
                ElseIf newChar = 122 Then
                    newChar = 116
                End If

                '(VW)
                If newChar = 118 Then
                        newChar = 119
                    ElseIf newChar = 119 Then
                        newChar = 118
                    End If
                    Console.WriteLine("Char after reflector: " + Chr(newChar).ToString())
                End If

            ''2nd time going through the rotors
            If Not onlyOne Then
                '3rd rotor
                contact = (newChar - 96) + (nudThirdRotor.Value - 1) + (nudRingSetting3.Value - 1)
                contact = cirkelLoop(contact, 1, 26)
                Console.WriteLine("Contact: " + contact.ToString())
                newChar -= Rotors(thirdRotor - 1)(1)(contact - 1)
                newChar = cirkelLoop(newChar, 97, 122)
                Console.WriteLine("Offset: -" + Rotors(thirdRotor - 1)(1)(contact - 1).ToString())
                Console.WriteLine("New char: " + Chr(newChar).ToString())

                '2nd rotor
                contact = (newChar - 96) + (nudSecondRotor.Value - 1) + (nudRingSetting2.Value - 1)
                contact = cirkelLoop(contact, 1, 26)
                Console.WriteLine("Contact: " + contact.ToString())
                newChar -= Rotors(secondRotor - 1)(1)(contact - 1)
                newChar = cirkelLoop(newChar, 97, 122)
                Console.WriteLine("Offset: -" + Rotors(secondRotor - 1)(1)(contact - 1).ToString())
                Console.WriteLine("New char: " + Chr(newChar).ToString())
            End If

            '1st rotor
            contact = (newChar - 96) + (nudFirstRotor.Value - 1) + (nudRingSetting1.Value - 1)
            contact = cirkelLoop(contact, 1, 26)
            Console.WriteLine("Contact: " + contact.ToString())
            newChar -= Rotors(firstRotor - 1)(1)(contact - 1)
            newChar = cirkelLoop(newChar, 97, 122)
            Console.WriteLine("Offset: -" + Rotors(firstRotor - 1)(1)(contact - 1).ToString())
            Console.WriteLine("New char: " + Chr(newChar).ToString())


            ''Plugboard
            If plugboardString.Contains(Chr(newChar)) Then
                Console.WriteLine("Character config in plugboard!")
                Dim plugboardArray As String() = Split(plugboardString, "/")
                Dim link As String = ""
                For i As Integer = 0 To plugboardArray.Length - 1
                    If plugboardArray(i).Contains(Chr(newChar)) Then
                        link = plugboardArray(i)
                    End If
                Next
                If link.IndexOf(Chr(newChar)) = 0 Then
                    newChar = Asc(link(2))
                ElseIf link.IndexOf(Chr(newChar)) = 2 Then
                    newChar = Asc(link(0))
                End If
                Console.WriteLine("New char: " + Chr(newChar).ToString())
            End If

            tbOutput.Text += Chr(newChar)
        ElseIf cbActive.Checked And lastAdded = 8 Then
            Console.WriteLine("Backspace")
        End If
    End Sub

    Private Sub btnRotorsChange_Click(sender As Object, e As EventArgs) Handles btnRotorsChange.Click
        ChangeRotors.Show()
    End Sub

    Private Sub btnPlugboardChange_Click(sender As Object, e As EventArgs) Handles btnPlugboardChange.Click
        Plugboard.Show()
    End Sub

    Public Function cirkelLoop(var As Integer, min As Integer, max As Integer)
        Dim difference = max - min + 1
        If var > max Then
            var -= difference
        ElseIf var < min Then
        var += difference
        End If
        Return var
    End Function
End Class