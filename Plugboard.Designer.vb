﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Plugboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.tbPBL1_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL1_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbUitleg = New System.Windows.Forms.Label()
        Me.gbPlugboard2 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbPBL2_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL2_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard3 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbPBL3_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL3_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard4 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbPBL4_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL4_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard5 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbPBL5_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL5_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard10 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbPBL10_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL10_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard9 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbPBL9_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL9_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard8 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbPBL8_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL8_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard7 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbPBL7_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL7_1 = New System.Windows.Forms.TextBox()
        Me.gbPlugboard6 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbPBL6_2 = New System.Windows.Forms.TextBox()
        Me.tbPBL6_1 = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.gbPlugboard1.SuspendLayout()
        Me.gbPlugboard2.SuspendLayout()
        Me.gbPlugboard3.SuspendLayout()
        Me.gbPlugboard4.SuspendLayout()
        Me.gbPlugboard5.SuspendLayout()
        Me.gbPlugboard10.SuspendLayout()
        Me.gbPlugboard9.SuspendLayout()
        Me.gbPlugboard8.SuspendLayout()
        Me.gbPlugboard7.SuspendLayout()
        Me.gbPlugboard6.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbPBL1_2
        '
        Me.tbPBL1_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL1_2.MaxLength = 1
        Me.tbPBL1_2.Name = "tbPBL1_2"
        Me.tbPBL1_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL1_2.TabIndex = 0
        '
        'tbPBL1_1
        '
        Me.tbPBL1_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL1_1.MaxLength = 1
        Me.tbPBL1_1.Name = "tbPBL1_1"
        Me.tbPBL1_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL1_1.TabIndex = 1
        '
        'gbPlugboard1
        '
        Me.gbPlugboard1.Controls.Add(Me.Label2)
        Me.gbPlugboard1.Controls.Add(Me.tbPBL1_2)
        Me.gbPlugboard1.Controls.Add(Me.tbPBL1_1)
        Me.gbPlugboard1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard1.Location = New System.Drawing.Point(18, 44)
        Me.gbPlugboard1.Name = "gbPlugboard1"
        Me.gbPlugboard1.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard1.TabIndex = 2
        Me.gbPlugboard1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Plugboard link 1"
        '
        'lbUitleg
        '
        Me.lbUitleg.AutoSize = True
        Me.lbUitleg.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbUitleg.Location = New System.Drawing.Point(12, 9)
        Me.lbUitleg.Name = "lbUitleg"
        Me.lbUitleg.Size = New System.Drawing.Size(501, 26)
        Me.lbUitleg.TabIndex = 3
        Me.lbUitleg.Text = "Enter two characters to link them to eachother"
        '
        'gbPlugboard2
        '
        Me.gbPlugboard2.Controls.Add(Me.Label3)
        Me.gbPlugboard2.Controls.Add(Me.tbPBL2_2)
        Me.gbPlugboard2.Controls.Add(Me.tbPBL2_1)
        Me.gbPlugboard2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard2.Location = New System.Drawing.Point(122, 44)
        Me.gbPlugboard2.Name = "gbPlugboard2"
        Me.gbPlugboard2.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard2.TabIndex = 3
        Me.gbPlugboard2.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Plugboard link 2"
        '
        'tbPBL2_2
        '
        Me.tbPBL2_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL2_2.MaxLength = 1
        Me.tbPBL2_2.Name = "tbPBL2_2"
        Me.tbPBL2_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL2_2.TabIndex = 0
        '
        'tbPBL2_1
        '
        Me.tbPBL2_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL2_1.MaxLength = 1
        Me.tbPBL2_1.Name = "tbPBL2_1"
        Me.tbPBL2_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL2_1.TabIndex = 1
        '
        'gbPlugboard3
        '
        Me.gbPlugboard3.Controls.Add(Me.Label4)
        Me.gbPlugboard3.Controls.Add(Me.tbPBL3_2)
        Me.gbPlugboard3.Controls.Add(Me.tbPBL3_1)
        Me.gbPlugboard3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard3.Location = New System.Drawing.Point(226, 44)
        Me.gbPlugboard3.Name = "gbPlugboard3"
        Me.gbPlugboard3.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard3.TabIndex = 3
        Me.gbPlugboard3.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Plugboard link 3"
        '
        'tbPBL3_2
        '
        Me.tbPBL3_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL3_2.MaxLength = 1
        Me.tbPBL3_2.Name = "tbPBL3_2"
        Me.tbPBL3_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL3_2.TabIndex = 0
        '
        'tbPBL3_1
        '
        Me.tbPBL3_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL3_1.MaxLength = 1
        Me.tbPBL3_1.Name = "tbPBL3_1"
        Me.tbPBL3_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL3_1.TabIndex = 1
        '
        'gbPlugboard4
        '
        Me.gbPlugboard4.Controls.Add(Me.Label5)
        Me.gbPlugboard4.Controls.Add(Me.tbPBL4_2)
        Me.gbPlugboard4.Controls.Add(Me.tbPBL4_1)
        Me.gbPlugboard4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard4.Location = New System.Drawing.Point(330, 44)
        Me.gbPlugboard4.Name = "gbPlugboard4"
        Me.gbPlugboard4.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard4.TabIndex = 3
        Me.gbPlugboard4.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Plugboard link 4"
        '
        'tbPBL4_2
        '
        Me.tbPBL4_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL4_2.MaxLength = 1
        Me.tbPBL4_2.Name = "tbPBL4_2"
        Me.tbPBL4_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL4_2.TabIndex = 0
        '
        'tbPBL4_1
        '
        Me.tbPBL4_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL4_1.MaxLength = 1
        Me.tbPBL4_1.Name = "tbPBL4_1"
        Me.tbPBL4_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL4_1.TabIndex = 1
        '
        'gbPlugboard5
        '
        Me.gbPlugboard5.Controls.Add(Me.Label6)
        Me.gbPlugboard5.Controls.Add(Me.tbPBL5_2)
        Me.gbPlugboard5.Controls.Add(Me.tbPBL5_1)
        Me.gbPlugboard5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard5.Location = New System.Drawing.Point(437, 44)
        Me.gbPlugboard5.Name = "gbPlugboard5"
        Me.gbPlugboard5.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard5.TabIndex = 3
        Me.gbPlugboard5.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Plugboard link 5"
        '
        'tbPBL5_2
        '
        Me.tbPBL5_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL5_2.MaxLength = 1
        Me.tbPBL5_2.Name = "tbPBL5_2"
        Me.tbPBL5_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL5_2.TabIndex = 0
        '
        'tbPBL5_1
        '
        Me.tbPBL5_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL5_1.MaxLength = 1
        Me.tbPBL5_1.Name = "tbPBL5_1"
        Me.tbPBL5_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL5_1.TabIndex = 1
        '
        'gbPlugboard10
        '
        Me.gbPlugboard10.Controls.Add(Me.Label1)
        Me.gbPlugboard10.Controls.Add(Me.tbPBL10_2)
        Me.gbPlugboard10.Controls.Add(Me.tbPBL10_1)
        Me.gbPlugboard10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard10.Location = New System.Drawing.Point(437, 104)
        Me.gbPlugboard10.Name = "gbPlugboard10"
        Me.gbPlugboard10.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard10.TabIndex = 5
        Me.gbPlugboard10.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Plugboard link 10"
        '
        'tbPBL10_2
        '
        Me.tbPBL10_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL10_2.MaxLength = 1
        Me.tbPBL10_2.Name = "tbPBL10_2"
        Me.tbPBL10_2.Size = New System.Drawing.Size(21, 18)
        Me.tbPBL10_2.TabIndex = 0
        '
        'tbPBL10_1
        '
        Me.tbPBL10_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL10_1.MaxLength = 1
        Me.tbPBL10_1.Name = "tbPBL10_1"
        Me.tbPBL10_1.Size = New System.Drawing.Size(21, 18)
        Me.tbPBL10_1.TabIndex = 1
        '
        'gbPlugboard9
        '
        Me.gbPlugboard9.Controls.Add(Me.Label10)
        Me.gbPlugboard9.Controls.Add(Me.tbPBL9_2)
        Me.gbPlugboard9.Controls.Add(Me.tbPBL9_1)
        Me.gbPlugboard9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard9.Location = New System.Drawing.Point(330, 104)
        Me.gbPlugboard9.Name = "gbPlugboard9"
        Me.gbPlugboard9.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard9.TabIndex = 6
        Me.gbPlugboard9.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 13)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "Plugboard link 9"
        '
        'tbPBL9_2
        '
        Me.tbPBL9_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL9_2.MaxLength = 1
        Me.tbPBL9_2.Name = "tbPBL9_2"
        Me.tbPBL9_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL9_2.TabIndex = 0
        '
        'tbPBL9_1
        '
        Me.tbPBL9_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL9_1.MaxLength = 1
        Me.tbPBL9_1.Name = "tbPBL9_1"
        Me.tbPBL9_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL9_1.TabIndex = 1
        '
        'gbPlugboard8
        '
        Me.gbPlugboard8.Controls.Add(Me.Label9)
        Me.gbPlugboard8.Controls.Add(Me.tbPBL8_2)
        Me.gbPlugboard8.Controls.Add(Me.tbPBL8_1)
        Me.gbPlugboard8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard8.Location = New System.Drawing.Point(226, 104)
        Me.gbPlugboard8.Name = "gbPlugboard8"
        Me.gbPlugboard8.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard8.TabIndex = 7
        Me.gbPlugboard8.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Plugboard link 8"
        '
        'tbPBL8_2
        '
        Me.tbPBL8_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL8_2.MaxLength = 1
        Me.tbPBL8_2.Name = "tbPBL8_2"
        Me.tbPBL8_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL8_2.TabIndex = 0
        '
        'tbPBL8_1
        '
        Me.tbPBL8_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL8_1.MaxLength = 1
        Me.tbPBL8_1.Name = "tbPBL8_1"
        Me.tbPBL8_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL8_1.TabIndex = 1
        '
        'gbPlugboard7
        '
        Me.gbPlugboard7.Controls.Add(Me.Label8)
        Me.gbPlugboard7.Controls.Add(Me.tbPBL7_2)
        Me.gbPlugboard7.Controls.Add(Me.tbPBL7_1)
        Me.gbPlugboard7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard7.Location = New System.Drawing.Point(122, 104)
        Me.gbPlugboard7.Name = "gbPlugboard7"
        Me.gbPlugboard7.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard7.TabIndex = 8
        Me.gbPlugboard7.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Plugboard link 7"
        '
        'tbPBL7_2
        '
        Me.tbPBL7_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL7_2.MaxLength = 1
        Me.tbPBL7_2.Name = "tbPBL7_2"
        Me.tbPBL7_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL7_2.TabIndex = 0
        '
        'tbPBL7_1
        '
        Me.tbPBL7_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL7_1.MaxLength = 1
        Me.tbPBL7_1.Name = "tbPBL7_1"
        Me.tbPBL7_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL7_1.TabIndex = 1
        '
        'gbPlugboard6
        '
        Me.gbPlugboard6.Controls.Add(Me.Label7)
        Me.gbPlugboard6.Controls.Add(Me.tbPBL6_2)
        Me.gbPlugboard6.Controls.Add(Me.tbPBL6_1)
        Me.gbPlugboard6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPlugboard6.Location = New System.Drawing.Point(18, 104)
        Me.gbPlugboard6.Name = "gbPlugboard6"
        Me.gbPlugboard6.Size = New System.Drawing.Size(98, 49)
        Me.gbPlugboard6.TabIndex = 4
        Me.gbPlugboard6.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Plugboard link 6"
        '
        'tbPBL6_2
        '
        Me.tbPBL6_2.Location = New System.Drawing.Point(48, 19)
        Me.tbPBL6_2.MaxLength = 1
        Me.tbPBL6_2.Name = "tbPBL6_2"
        Me.tbPBL6_2.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL6_2.TabIndex = 0
        '
        'tbPBL6_1
        '
        Me.tbPBL6_1.Location = New System.Drawing.Point(21, 19)
        Me.tbPBL6_1.MaxLength = 1
        Me.tbPBL6_1.Name = "tbPBL6_1"
        Me.tbPBL6_1.Size = New System.Drawing.Size(21, 20)
        Me.tbPBL6_1.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(170, 160)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(202, 27)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Plugboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ClientSize = New System.Drawing.Size(547, 199)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.gbPlugboard10)
        Me.Controls.Add(Me.gbPlugboard5)
        Me.Controls.Add(Me.gbPlugboard9)
        Me.Controls.Add(Me.gbPlugboard4)
        Me.Controls.Add(Me.gbPlugboard8)
        Me.Controls.Add(Me.gbPlugboard3)
        Me.Controls.Add(Me.gbPlugboard7)
        Me.Controls.Add(Me.gbPlugboard2)
        Me.Controls.Add(Me.gbPlugboard6)
        Me.Controls.Add(Me.lbUitleg)
        Me.Controls.Add(Me.gbPlugboard1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Plugboard"
        Me.ShowIcon = False
        Me.TopMost = True
        Me.gbPlugboard1.ResumeLayout(False)
        Me.gbPlugboard1.PerformLayout()
        Me.gbPlugboard2.ResumeLayout(False)
        Me.gbPlugboard2.PerformLayout()
        Me.gbPlugboard3.ResumeLayout(False)
        Me.gbPlugboard3.PerformLayout()
        Me.gbPlugboard4.ResumeLayout(False)
        Me.gbPlugboard4.PerformLayout()
        Me.gbPlugboard5.ResumeLayout(False)
        Me.gbPlugboard5.PerformLayout()
        Me.gbPlugboard10.ResumeLayout(False)
        Me.gbPlugboard10.PerformLayout()
        Me.gbPlugboard9.ResumeLayout(False)
        Me.gbPlugboard9.PerformLayout()
        Me.gbPlugboard8.ResumeLayout(False)
        Me.gbPlugboard8.PerformLayout()
        Me.gbPlugboard7.ResumeLayout(False)
        Me.gbPlugboard7.PerformLayout()
        Me.gbPlugboard6.ResumeLayout(False)
        Me.gbPlugboard6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbPBL1_2 As TextBox
    Friend WithEvents tbPBL1_1 As TextBox
    Friend WithEvents gbPlugboard1 As GroupBox
    Friend WithEvents lbUitleg As Label
    Friend WithEvents gbPlugboard2 As GroupBox
    Friend WithEvents tbPBL2_2 As TextBox
    Friend WithEvents tbPBL2_1 As TextBox
    Friend WithEvents gbPlugboard3 As GroupBox
    Friend WithEvents tbPBL3_2 As TextBox
    Friend WithEvents tbPBL3_1 As TextBox
    Friend WithEvents gbPlugboard4 As GroupBox
    Friend WithEvents tbPBL4_2 As TextBox
    Friend WithEvents tbPBL4_1 As TextBox
    Friend WithEvents gbPlugboard5 As GroupBox
    Friend WithEvents tbPBL5_2 As TextBox
    Friend WithEvents tbPBL5_1 As TextBox
    Friend WithEvents gbPlugboard10 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents tbPBL10_2 As TextBox
    Friend WithEvents tbPBL10_1 As TextBox
    Friend WithEvents gbPlugboard9 As GroupBox
    Friend WithEvents tbPBL9_2 As TextBox
    Friend WithEvents tbPBL9_1 As TextBox
    Friend WithEvents gbPlugboard8 As GroupBox
    Friend WithEvents tbPBL8_2 As TextBox
    Friend WithEvents tbPBL8_1 As TextBox
    Friend WithEvents gbPlugboard7 As GroupBox
    Friend WithEvents tbPBL7_2 As TextBox
    Friend WithEvents tbPBL7_1 As TextBox
    Friend WithEvents gbPlugboard6 As GroupBox
    Friend WithEvents tbPBL6_2 As TextBox
    Friend WithEvents tbPBL6_1 As TextBox
    Friend WithEvents btnSave As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
End Class
