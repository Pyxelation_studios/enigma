﻿Public Class ChangeRotors
    Public firstRotor As Integer
    Public secondRotor As Integer
    Public ThirdRotor As Integer

    Private Sub Rotors_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        nudFirstRotor.Value = enigmaEncoder.firstRotor
        nudSecondRotor.Value = enigmaEncoder.secondRotor
        nudThirdRotor.Value = enigmaEncoder.thirdRotor
    End Sub

    Private Overloads Sub Close() Handles btnSave.Click
        enigmaEncoder.firstRotor = nudFirstRotor.Value
        enigmaEncoder.secondRotor = nudSecondRotor.Value
        enigmaEncoder.thirdRotor = nudThirdRotor.Value
        MyBase.Close()
    End Sub

    Private Sub nudRotor_ValueChanged(sender As Object, e As EventArgs) Handles nudFirstRotor.ValueChanged, nudSecondRotor.ValueChanged, nudThirdRotor.ValueChanged
        If nudFirstRotor.Value = nudSecondRotor.Value Or nudFirstRotor.Value = nudThirdRotor.Value Then
            nudFirstRotor.Value += 1
        End If
        If nudSecondRotor.Value = nudFirstRotor.Value Or nudSecondRotor.Value = nudThirdRotor.Value Then
            nudSecondRotor.Value += 1
        End If
        If nudThirdRotor.Value = nudFirstRotor.Value Or nudThirdRotor.Value = nudSecondRotor.Value Then
            nudThirdRotor.Value += 1
        End If
        If sender.value > 5 Then
            sender.value = 1
        ElseIf sender.value < 1 Then
            sender.value = 5
        End If
    End Sub
End Class