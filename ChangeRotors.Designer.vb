﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChangeRotors
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbRotorsChange = New System.Windows.Forms.GroupBox()
        Me.nudThirdRotor = New System.Windows.Forms.NumericUpDown()
        Me.lbThirdRotor = New System.Windows.Forms.Label()
        Me.nudSecondRotor = New System.Windows.Forms.NumericUpDown()
        Me.lbSecondRotor = New System.Windows.Forms.Label()
        Me.nudFirstRotor = New System.Windows.Forms.NumericUpDown()
        Me.lbFirstRotor = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.gbRotorsChange.SuspendLayout()
        CType(Me.nudThirdRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudSecondRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFirstRotor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbRotorsChange
        '
        Me.gbRotorsChange.Controls.Add(Me.nudThirdRotor)
        Me.gbRotorsChange.Controls.Add(Me.lbThirdRotor)
        Me.gbRotorsChange.Controls.Add(Me.nudSecondRotor)
        Me.gbRotorsChange.Controls.Add(Me.lbSecondRotor)
        Me.gbRotorsChange.Controls.Add(Me.nudFirstRotor)
        Me.gbRotorsChange.Controls.Add(Me.lbFirstRotor)
        Me.gbRotorsChange.Location = New System.Drawing.Point(13, 13)
        Me.gbRotorsChange.Name = "gbRotorsChange"
        Me.gbRotorsChange.Size = New System.Drawing.Size(283, 78)
        Me.gbRotorsChange.TabIndex = 0
        Me.gbRotorsChange.TabStop = False
        Me.gbRotorsChange.Text = "Change the rotors"
        '
        'nudThirdRotor
        '
        Me.nudThirdRotor.Location = New System.Drawing.Point(195, 37)
        Me.nudThirdRotor.Maximum = New Decimal(New Integer() {6, 0, 0, 0})
        Me.nudThirdRotor.Name = "nudThirdRotor"
        Me.nudThirdRotor.Size = New System.Drawing.Size(74, 20)
        Me.nudThirdRotor.TabIndex = 5
        Me.nudThirdRotor.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'lbThirdRotor
        '
        Me.lbThirdRotor.AutoSize = True
        Me.lbThirdRotor.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lbThirdRotor.Location = New System.Drawing.Point(192, 16)
        Me.lbThirdRotor.Name = "lbThirdRotor"
        Me.lbThirdRotor.Size = New System.Drawing.Size(82, 17)
        Me.lbThirdRotor.TabIndex = 4
        Me.lbThirdRotor.Text = "Third rotor:"
        '
        'nudSecondRotor
        '
        Me.nudSecondRotor.Location = New System.Drawing.Point(96, 37)
        Me.nudSecondRotor.Maximum = New Decimal(New Integer() {6, 0, 0, 0})
        Me.nudSecondRotor.Name = "nudSecondRotor"
        Me.nudSecondRotor.Size = New System.Drawing.Size(74, 20)
        Me.nudSecondRotor.TabIndex = 3
        Me.nudSecondRotor.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'lbSecondRotor
        '
        Me.lbSecondRotor.AutoSize = True
        Me.lbSecondRotor.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lbSecondRotor.Location = New System.Drawing.Point(89, 16)
        Me.lbSecondRotor.Name = "lbSecondRotor"
        Me.lbSecondRotor.Size = New System.Drawing.Size(97, 17)
        Me.lbSecondRotor.TabIndex = 2
        Me.lbSecondRotor.Text = "Second rotor:"
        '
        'nudFirstRotor
        '
        Me.nudFirstRotor.Location = New System.Drawing.Point(9, 37)
        Me.nudFirstRotor.Maximum = New Decimal(New Integer() {6, 0, 0, 0})
        Me.nudFirstRotor.Name = "nudFirstRotor"
        Me.nudFirstRotor.Size = New System.Drawing.Size(74, 20)
        Me.nudFirstRotor.TabIndex = 1
        Me.nudFirstRotor.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lbFirstRotor
        '
        Me.lbFirstRotor.AutoSize = True
        Me.lbFirstRotor.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lbFirstRotor.Location = New System.Drawing.Point(6, 16)
        Me.lbFirstRotor.Name = "lbFirstRotor"
        Me.lbFirstRotor.Size = New System.Drawing.Size(77, 17)
        Me.lbFirstRotor.TabIndex = 0
        Me.lbFirstRotor.Text = "First rotor:"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(13, 97)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(283, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'ChangeRotors
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ClientSize = New System.Drawing.Size(308, 125)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.gbRotorsChange)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ChangeRotors"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.TopMost = True
        Me.gbRotorsChange.ResumeLayout(False)
        Me.gbRotorsChange.PerformLayout()
        CType(Me.nudThirdRotor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudSecondRotor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFirstRotor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbRotorsChange As GroupBox
    Friend WithEvents lbFirstRotor As Label
    Friend WithEvents nudFirstRotor As NumericUpDown
    Friend WithEvents nudSecondRotor As NumericUpDown
    Friend WithEvents lbSecondRotor As Label
    Friend WithEvents nudThirdRotor As NumericUpDown
    Friend WithEvents lbThirdRotor As Label
    Friend WithEvents btnSave As Button
End Class
