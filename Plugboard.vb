﻿Public Class Plugboard
    Public plugboardString As String
    Private Sub plugboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        plugboardString = enigmaEncoder.plugboardString
        Dim plugboardArray As String() = Split(plugboardString, "/")
        If Not plugboardString = "" Then
            For i As Integer = 0 To plugboardArray.Length - 1
                Select Case i
                    Case 0
                        Dim link As String = plugboardArray(i)
                        tbPBL1_1.Text = link(0)
                        tbPBL1_2.Text = link(2)

                    Case 1
                        Dim link As String = plugboardArray(i)
                        tbPBL2_1.Text = link(0)
                        tbPBL2_2.Text = link(2)

                    Case 2
                        Dim link As String = plugboardArray(i)
                        tbPBL3_1.Text = link(0)
                        tbPBL3_2.Text = link(2)

                    Case 3
                        Dim link As String = plugboardArray(i)
                        tbPBL4_1.Text = link(0)
                        tbPBL4_2.Text = link(2)

                    Case 4
                        Dim link As String = plugboardArray(i)
                        tbPBL5_1.Text = link(0)
                        tbPBL5_2.Text = link(2)

                    Case 5
                        Dim link As String = plugboardArray(i)
                        tbPBL6_1.Text = link(0)
                        tbPBL6_2.Text = link(2)

                    Case 6
                        Dim link As String = plugboardArray(i)
                        tbPBL7_1.Text = link(0)
                        tbPBL7_2.Text = link(2)

                    Case 7
                        Dim link As String = plugboardArray(i)
                        tbPBL8_1.Text = link(0)
                        tbPBL8_2.Text = link(2)

                    Case 8
                        Dim link As String = plugboardArray(i)
                        tbPBL9_1.Text = link(0)
                        tbPBL9_2.Text = link(2)

                    Case 9
                        Dim link As String = plugboardArray(i)
                        tbPBL10_1.Text = link(0)
                        tbPBL10_2.Text = link(2)

                End Select 'transfer values
            Next
        End If
    End Sub

    Private Sub tbInput_characterCheck(sender As Object, e As KeyPressEventArgs) Handles tbPBL1_1.KeyPress, tbPBL1_2.KeyPress, tbPBL2_1.KeyPress, tbPBL2_2.KeyPress, tbPBL3_1.KeyPress, tbPBL3_2.KeyPress, tbPBL4_1.KeyPress, tbPBL4_2.KeyPress, tbPBL5_1.KeyPress, tbPBL5_2.KeyPress, tbPBL6_1.KeyPress, tbPBL6_2.KeyPress, tbPBL7_1.KeyPress, tbPBL7_2.KeyPress, tbPBL8_1.KeyPress, tbPBL8_2.KeyPress, tbPBL9_1.KeyPress, tbPBL9_2.KeyPress, tbPBL10_1.KeyPress, tbPBL10_2.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then 'not backspace
            Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Overloads Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        plugboardString = ""
        If tbPBL1_1.Text.Length > 0 And tbPBL1_2.Text.Length > 0 And Not tbPBL1_1.Text = tbPBL1_2.Text Then
            If Not plugboardString.Contains(tbPBL1_1.Text) And Not plugboardString.Contains(tbPBL1_2.Text) Then
                plugboardString += tbPBL1_1.Text + "-" + tbPBL1_2.Text + "/"
            End If
        End If

        If tbPBL2_1.Text.Length > 0 And tbPBL2_2.Text.Length > 0 And Not tbPBL2_1.Text = tbPBL2_2.Text Then
            If Not plugboardString.Contains(tbPBL2_1.Text) And Not plugboardString.Contains(tbPBL2_2.Text) Then
                plugboardString += tbPBL2_1.Text + "-" + tbPBL2_2.Text + "/"
            End If
        End If

        If tbPBL3_1.Text.Length > 0 And tbPBL3_2.Text.Length > 0 And Not tbPBL3_1.Text = tbPBL3_2.Text Then
            If Not plugboardString.Contains(tbPBL3_1.Text) And Not plugboardString.Contains(tbPBL3_2.Text) Then
                plugboardString += tbPBL3_1.Text + "-" + tbPBL3_2.Text + "/"
            End If
        End If

        If tbPBL4_1.Text.Length > 0 And tbPBL4_2.Text.Length > 0 And Not tbPBL4_1.Text = tbPBL4_2.Text Then
            If Not plugboardString.Contains(tbPBL4_1.Text) And Not plugboardString.Contains(tbPBL4_2.Text) Then
                plugboardString += tbPBL4_1.Text + "-" + tbPBL4_2.Text + "/"
            End If
        End If

        If tbPBL5_1.Text.Length > 0 And tbPBL5_2.Text.Length > 0 And Not tbPBL5_1.Text = tbPBL5_2.Text Then
            If Not plugboardString.Contains(tbPBL5_1.Text) And Not plugboardString.Contains(tbPBL5_2.Text) Then
                plugboardString += tbPBL5_1.Text + "-" + tbPBL5_2.Text + "/"
            End If
        End If

        If tbPBL6_1.Text.Length > 0 And tbPBL6_2.Text.Length > 0 And Not tbPBL6_1.Text = tbPBL6_2.Text Then
            If Not plugboardString.Contains(tbPBL6_1.Text) And Not plugboardString.Contains(tbPBL6_2.Text) Then
                plugboardString += tbPBL6_1.Text + "-" + tbPBL6_2.Text + "/"
            End If
        End If

        If tbPBL7_1.Text.Length > 0 And tbPBL7_2.Text.Length > 0 And Not tbPBL7_1.Text = tbPBL7_2.Text Then
            If Not plugboardString.Contains(tbPBL7_1.Text) And Not plugboardString.Contains(tbPBL7_2.Text) Then
                plugboardString += tbPBL7_1.Text + "-" + tbPBL7_2.Text + "/"
            End If
        End If

        If tbPBL8_1.Text.Length > 0 And tbPBL8_2.Text.Length > 0 And Not tbPBL8_1.Text = tbPBL8_2.Text Then
            If Not plugboardString.Contains(tbPBL8_1.Text) And Not plugboardString.Contains(tbPBL8_2.Text) Then
                plugboardString += tbPBL8_1.Text + "-" + tbPBL8_2.Text + "/"
            End If
        End If

        If tbPBL9_1.Text.Length > 0 And tbPBL9_2.Text.Length > 0 And Not tbPBL9_1.Text = tbPBL9_2.Text Then
            If Not plugboardString.Contains(tbPBL9_1.Text) And Not plugboardString.Contains(tbPBL9_2.Text) Then
                plugboardString += tbPBL9_1.Text + "-" + tbPBL9_2.Text + "/"
            End If
        End If

        If tbPBL10_1.Text.Length > 0 And tbPBL10_2.Text.Length > 0 And Not tbPBL10_1.Text = tbPBL10_2.Text Then
            If Not plugboardString.Contains(tbPBL10_1.Text) And Not plugboardString.Contains(tbPBL10_2.Text) Then
                plugboardString += tbPBL10_1.Text + "-" + tbPBL10_2.Text
            End If
        End If

        If plugboardString(plugboardString.Length - 1) = "/" Then
            Dim str = ""
            For i As Integer = 0 To plugboardString.Length - 2
                str += plugboardString(i)
            Next
            plugboardString = str
        End If
        Console.WriteLine("Plugboard string: " + plugboardString)
        enigmaEncoder.plugboardString = plugboardString
        MyBase.Close()
    End Sub
End Class